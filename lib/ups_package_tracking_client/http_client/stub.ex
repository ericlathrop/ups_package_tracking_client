defmodule UpsPackageTrackingClient.HttpClient.Stub do
  @behaviour UpsPackageTrackingClient.HttpClient

  @impl UpsPackageTrackingClient.HttpClient
  def get(url, headers, _options) do
    %URI{path: "/track/v1/details/" <> tracking_number, query: query} = URI.parse(url)
    %{"locale" => locale} = URI.decode_query(query)
    access_key = get_header(headers, "AccessLicenseNumber")
    fake_data(tracking_number, access_key, locale)
  end

  defp get_header(headers, key) do
    header =
      Enum.find(headers, fn
        {^key, _value} -> true
        _ -> false
      end)

    case header do
      {^key, value} -> value
      _ -> nil
    end
  end

  defp fake_data(_tracking_number, "INVALID", _locale) do
    fake_response(401, "invalid_access_key_response")
  end

  defp fake_data("1Z5338FF0107231059", "VALID", _locale) do
    fake_response(200, "small_package_response")
  end

  defp fake_data("92055900100111152280003029", "VALID", _locale) do
    fake_response(200, "mail_innovations_response")
  end

  defp fake_data("7798339175", "VALID", _locale) do
    fake_response(200, "freight_response")
  end

  defp fake_data("572254454", "VALID", _locale) do
    fake_response(200, "overnight_response")
  end

  defp fake_data(_tracking_number, _access_key, _locale) do
    {:error, "can't find fake data"}
  end

  defp fake_response(status_code, data_file) do
    {:ok, json} = File.read("test/data/#{data_file}.json")
    {:ok, %HTTPoison.Response{status_code: status_code, body: json}}
  end
end
