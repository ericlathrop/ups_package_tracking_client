defmodule UpsPackageTrackingClient.TrackingResponse do
  use Ecto.Schema
  import Ecto.Changeset
  alias UpsPackageTrackingClient.TrackingResponse

  @primary_key false

  embedded_schema do
    embeds_many(:shipment, UpsPackageTrackingClient.Shipment)
  end

  @type t() :: %UpsPackageTrackingClient.TrackingResponse{
          shipment: [UpsPackageTrackingClient.Shipment.t()]
        }

  def changeset(tracking_response, attrs \\ %{}) do
    tracking_response
    |> cast(attrs, [])
    |> cast_embed(:shipment)
    |> validate_required([:shipment])
  end

  def build(attrs) do
    struct(TrackingResponse)
    |> changeset(attrs)
    |> apply_action(:build)
  end
end
