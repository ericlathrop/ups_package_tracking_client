defmodule UpsPackageTrackingClient do
  @moduledoc """
  Public interface for the library.
  """

  @production_hostname "onlinetools.ups.com"
  @http_client Application.get_env(
                 :ups_package_tracking_client,
                 :http_client,
                 UpsPackageTrackingClient.HttpClient.HTTPoison
               )

  defp api_hostname() do
    Application.get_env(:ups_package_tracking_client, :api_hostname, @production_hostname)
  end

  @doc """
  Query UPS for the tracking status of a tracking number
  """
  @spec track(String.t(), String.t(), String.t()) ::
          {:ok, UpsPackageTrackingClient.TrackingResponse.t()} | {:error, any()}
  def track(tracking_number, access_key, locale \\ "en_US") do
    with {:ok, %HTTPoison.Response{status_code: 200, body: body}} <-
           @http_client.get(
             "https://#{api_hostname()}/track/v1/details/#{tracking_number}?locale=#{locale}",
             [
               {"AccessLicenseNumber", access_key},
               {"Content-Type", "application/json"},
               {"Accept", "application/json"}
             ],
             []
           ),
         {:ok, %{"trackResponse" => response}} <- Jason.decode(body) do
      response
      |> UpsPackageTrackingClient.TrackingResponse.build()
    else
      {:ok, %HTTPoison.Response{status_code: status_code, body: body}}
      when status_code >= 400 and status_code < 600 ->
        {:ok, %{"response" => %{"errors" => errors}}} = Jason.decode(body)
        {:error, errors}
    end
  end
end
