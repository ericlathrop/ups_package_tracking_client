defmodule UpsPackageTrackingClient.HttpClient do
  @type header :: {String.t(), String.t()}

  @callback get(String.t(), [header], list()) :: {:ok, HTTPoison.Response} | {:error, any()}
end
