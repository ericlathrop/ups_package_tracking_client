defmodule UpsPackageTrackingClientTest do
  use ExUnit.Case
  doctest UpsPackageTrackingClient

  describe "track" do
    test "with invalid access_key returns error" do
      assert {:error, [%{"code" => _code, "message" => _message}]} =
               UpsPackageTrackingClient.track("", "INVALID")
    end

    test "with small package tracking_number and valid access_key returns TrackingResponse" do
      assert {:ok,
              %UpsPackageTrackingClient.TrackingResponse{
                shipment: [
                  %UpsPackageTrackingClient.Shipment{
                    package: [
                      %UpsPackageTrackingClient.Package{
                        activity: [
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2019-11-21],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "BALDWIN",
                                country: "US",
                                postal_code: nil,
                                state_province: "MD"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "48",
                              description: "DeliveryAttempted",
                              type: "X"
                            },
                            time: ~T[14:04:00]
                          },
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2019-11-21],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "Sparks",
                                country: "US",
                                postal_code: nil,
                                state_province: "MD"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "48",
                              description:
                                "Thereceiverwasnotavailablefordelivery.We'llmakeasecondattemptthenextbusinessday.",
                              type: "X"
                            },
                            time: ~T[13:58:00]
                          },
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2019-11-19],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: nil,
                                country: "US",
                                postal_code: nil,
                                state_province: nil
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "MP",
                              description: "OrderProcessed:ReadyforUPS",
                              type: "M"
                            },
                            time: ~T[13:26:42]
                          }
                        ],
                        delivery_date: [],
                        delivery_time: nil,
                        tracking_number: "1Z5338FF0107231059"
                      }
                    ]
                  }
                ]
              }} = UpsPackageTrackingClient.track("1Z5338FF0107231059", "VALID")
    end

    test "with mail innovations tracking_number and valid access_key returns TrackingResponse" do
      assert {:ok,
              %UpsPackageTrackingClient.TrackingResponse{
                shipment: [
                  %UpsPackageTrackingClient.Shipment{
                    package: [
                      %UpsPackageTrackingClient.Package{
                        activity: [
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2020-01-16],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "Butner",
                                country: "US",
                                postal_code: "27509",
                                state_province: "NC"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "ZW",
                              description: "PostagePaid/Readyfordestinationpostofficeentry",
                              type: "I"
                            },
                            time: ~T[10:37:00]
                          },
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2020-01-16],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "Butner",
                                country: "US",
                                postal_code: "27509",
                                state_province: "NC"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "ZR",
                              description:
                                "PackagedepartedUPSMailInnovationsfacilityenroutetoUSPSforinduction",
                              type: "I"
                            },
                            time: ~T[10:37:00]
                          },
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2020-01-16],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "Butner",
                                country: "US",
                                postal_code: "27509",
                                state_province: "NC"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "ZT",
                              description: "PackageprocessedbyUPSMailInnovationsoriginfacility",
                              type: "I"
                            },
                            time: ~T[10:34:00]
                          },
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2020-01-16],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "Butner",
                                country: "US",
                                postal_code: "27509",
                                state_province: "NC"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "ZS",
                              description: "PackagereceivedforprocessingbyUPSMailInnovations",
                              type: "I"
                            },
                            time: ~T[09:55:00]
                          },
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2020-01-16],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "DURHAM",
                                country: "US",
                                postal_code: "27713",
                                state_province: "NC"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "VS",
                              description: "ShipmenttenderedtoUPSMailInnovations",
                              type: "P"
                            },
                            time: ~T[02:02:00]
                          }
                        ],
                        delivery_date: [
                          %UpsPackageTrackingClient.DeliveryDate{
                            date: ~D[2020-01-23],
                            type: "SDD"
                          }
                        ],
                        delivery_time: nil,
                        tracking_number: "92055900100111152280003029"
                      }
                    ]
                  }
                ]
              }} = UpsPackageTrackingClient.track("92055900100111152280003029", "VALID")
    end

    test "with freight tracking_number and valid access_key returns TrackingResponse" do
      assert {:ok,
              %UpsPackageTrackingClient.TrackingResponse{
                shipment: [
                  %UpsPackageTrackingClient.Shipment{
                    package: [
                      %UpsPackageTrackingClient.Package{
                        activity: [
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2020-07-06],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "Vancouver",
                                country: "CA",
                                postal_code: nil,
                                state_province: "BC"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "006",
                              description: "ETA at Port of Discharge",
                              type: "I"
                            },
                            time: ~T[00:01:00]
                          },
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2020-06-15],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "Tokyo",
                                country: "JP",
                                postal_code: nil,
                                state_province: "13"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "045",
                              description: "Documents received from shipper",
                              type: nil
                            },
                            time: ~T[10:27:02]
                          },
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2020-06-15],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "Tokyo",
                                country: "JP",
                                postal_code: nil,
                                state_province: "13"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "003",
                              description: "Date Available to Ship",
                              type: nil
                            },
                            time: ~T[10:27:02]
                          },
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2020-06-14],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "Kobe",
                                country: "JP",
                                postal_code: nil,
                                state_province: "28"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "004",
                              description: "ETD from Load Port",
                              type: nil
                            },
                            time: ~T[14:18:00]
                          },
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2020-06-14],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "Kobe",
                                country: "JP",
                                postal_code: nil,
                                state_province: "28"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "005",
                              description: "Departure",
                              type: nil
                            },
                            time: ~T[14:18:00]
                          },
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2020-06-11],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "Osaka",
                                country: "JP",
                                postal_code: nil,
                                state_province: "27"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "072",
                              description: "Planned pickup date",
                              type: nil
                            },
                            time: ~T[10:00:00]
                          },
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2020-06-11],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "Tokyo",
                                country: "JP",
                                postal_code: nil,
                                state_province: "13"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "002",
                              description: "Received into UPS possession",
                              type: nil
                            },
                            time: ~T[10:00:00]
                          },
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2020-06-04],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "Tokyo",
                                country: "JP",
                                postal_code: nil,
                                state_province: "13"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: "037",
                              description: "Booking Received from Client",
                              type: nil
                            },
                            time: ~T[02:38:12]
                          }
                        ],
                        delivery_date: [
                          %UpsPackageTrackingClient.DeliveryDate{
                            date: ~D[2020-07-21],
                            type: "SDD"
                          }
                        ],
                        delivery_time: nil,
                        tracking_number: "7798339175"
                      }
                    ]
                  }
                ]
              }} = UpsPackageTrackingClient.track("7798339175", "VALID")
    end

    test "with overnight tracking_number and valid access_key returns TrackingResponse" do
      assert {:ok,
              %UpsPackageTrackingClient.TrackingResponse{
                shipment: [
                  %UpsPackageTrackingClient.Shipment{
                    package: [
                      %UpsPackageTrackingClient.Package{
                        activity: [
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2019-12-07],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "Truckload",
                                country: "US",
                                postal_code: nil,
                                state_province: "VA"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: nil,
                              description: "Shipmenthasbeendeliveredtoconsignee",
                              type: "D"
                            },
                            time: ~T[10:45:00]
                          },
                          %UpsPackageTrackingClient.Activity{
                            date: ~D[2019-12-07],
                            location: %UpsPackageTrackingClient.Location{
                              address: %UpsPackageTrackingClient.Address{
                                city: "Truckload",
                                country: "US",
                                postal_code: nil,
                                state_province: "VA"
                              }
                            },
                            status: %UpsPackageTrackingClient.ActivityStatus{
                              code: nil,
                              description: "Shipmenthasbeenpickedup",
                              type: "I"
                            },
                            time: ~T[08:00:00]
                          }
                        ],
                        delivery_date: [
                          %UpsPackageTrackingClient.DeliveryDate{
                            date: ~D[2019-12-07],
                            type: "DEL"
                          },
                          %UpsPackageTrackingClient.DeliveryDate{
                            date: ~D[2019-12-10],
                            type: "SDD"
                          }
                        ],
                        delivery_time: %UpsPackageTrackingClient.DeliveryTime{
                          end_time: nil,
                          start_time: nil,
                          type: "EOD"
                        },
                        tracking_number: "572254454"
                      }
                    ]
                  }
                ]
              }} = UpsPackageTrackingClient.track("572254454", "VALID")
    end
  end
end
