defmodule UpsPackageTrackingClient.Activity do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:date, UpsPackageTrackingClient.UpsDate)
    embeds_one(:location, UpsPackageTrackingClient.Location)
    field(:time, UpsPackageTrackingClient.UpsTime)
    embeds_one(:status, UpsPackageTrackingClient.ActivityStatus)
  end

  @type t() :: %UpsPackageTrackingClient.Activity{
          date: Date.t(),
          location: UpsPackageTrackingClient.Location.t(),
          time: Time.t(),
          status: UpsPackageTrackingClient.ActivityStatus.t()
        }

  def changeset(activity, attrs \\ %{}) do
    activity
    |> cast(attrs, [:date, :time])
    |> cast_embed(:location)
    |> cast_embed(:status)
    |> validate_required([:date, :status, :time])
  end
end
