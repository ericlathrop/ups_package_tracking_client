defmodule UpsPackageTrackingClient.UpsDate do
  use Ecto.Type
  def type, do: :date

  def cast(
        <<year_string::binary-size(4), month_string::binary-size(2), day_string::binary-size(2)>>
      ) do
    with {year, _} <- Integer.parse(year_string),
         {month, _} <- Integer.parse(month_string),
         {day, _} <- Integer.parse(day_string),
         {:ok, date} <- Date.new(year, month, day) do
      {:ok, date}
    else
      _ -> :error
    end
  end

  def cast(_), do: :error

  def load(erl_date = {_year, _month, _day}), do: {:ok, Date.from_erl(erl_date)}

  def dump(date = %Date{}), do: {:ok, Date.to_erl(date)}
  def dump(_), do: :error
end
