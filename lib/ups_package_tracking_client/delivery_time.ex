defmodule UpsPackageTrackingClient.DeliveryTime do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:end_time, UpsPackageTrackingClient.UpsTime)
    field(:start_time, UpsPackageTrackingClient.UpsTime)
    field(:type, :string)
  end

  @type t() :: %UpsPackageTrackingClient.DeliveryTime{
          end_time: Time.t(),
          start_time: Time.t(),
          type: String.t()
        }

  def changeset(delivery_time, attrs \\ %{}) do
    attrs =
      attrs
      |> UpsPackageTrackingClient.Map.rename_key("endTime", "end_time")
      |> UpsPackageTrackingClient.Map.rename_key("startTime", "start_time")

    delivery_time
    |> cast(attrs, [:end_time, :start_time, :type])
    # FIXME: docs say startTime is required, but it is missing on some responses
    |> validate_required([:type])
    |> validate_length(:type, is: 3)
  end
end
