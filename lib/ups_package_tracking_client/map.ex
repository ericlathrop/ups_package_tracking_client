defmodule UpsPackageTrackingClient.Map do
  @moduledoc false

  def rename_key(map, old_key, new_key) do
    case Map.pop(map, old_key) do
      {nil, map} -> map
      {value, map} -> Map.put(map, new_key, value)
    end
  end
end
