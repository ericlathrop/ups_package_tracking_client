defmodule UpsPackageTrackingClient.Package do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:tracking_number, :string)
    embeds_many(:activity, UpsPackageTrackingClient.Activity)
    embeds_many(:delivery_date, UpsPackageTrackingClient.DeliveryDate)
    embeds_one(:delivery_time, UpsPackageTrackingClient.DeliveryTime)
  end

  @type t() :: %UpsPackageTrackingClient.Package{
          tracking_number: String.t(),
          activity: [UpsPackageTrackingClient.Activity.t()],
          delivery_date: [UpsPackageTrackingClient.DeliveryDate.t()],
          delivery_time: UpsPackageTrackingClient.DeliveryTime.t()
        }

  def changeset(package, attrs \\ %{}) do
    attrs =
      attrs
      |> UpsPackageTrackingClient.Map.rename_key("trackingNumber", "tracking_number")
      |> UpsPackageTrackingClient.Map.rename_key("deliveryDate", "delivery_date")
      |> UpsPackageTrackingClient.Map.rename_key("deliveryTime", "delivery_time")

    package
    |> cast(attrs, [:tracking_number])
    |> cast_embed(:activity)
    |> cast_embed(:delivery_date)
    |> cast_embed(:delivery_time)
    |> validate_required([:tracking_number])
    |> validate_length(:tracking_number, max: 34)
  end
end
