defmodule UpsPackageTrackingClient.HttpClient.HTTPoison do
  @behaviour UpsPackageTrackingClient.HttpClient

  @impl UpsPackageTrackingClient.HttpClient
  defdelegate get(urls, headers, options), to: HTTPoison
end
