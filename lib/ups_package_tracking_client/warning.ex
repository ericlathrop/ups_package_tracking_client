defmodule UpsPackageTrackingClient.Warning do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:code, :string)
    field(:message, :string)
  end

  @type t() :: %UpsPackageTrackingClient.Warning{
          code: String.t(),
          message: String.t()
        }

  def changeset(package, attrs \\ %{}) do
    package
    |> cast(attrs, [:code, :message])
    |> validate_required([:code, :message])
  end
end
