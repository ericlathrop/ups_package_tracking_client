defmodule UpsPackageTrackingClient.Shipment do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    embeds_many(:package, UpsPackageTrackingClient.Package)
    embeds_many(:warnings, UpsPackageTrackingClient.Warning)
  end

  @type t() :: %UpsPackageTrackingClient.Shipment{
          package: [UpsPackageTrackingClient.Package.t()],
          warnings: [UpsPackageTrackingClient.Warning.t()]
        }

  def changeset(shipment, attrs \\ %{}) do
    shipment
    |> cast(attrs, [])
    |> cast_embed(:package)
    |> cast_embed(:warnings)
    |> validate_required([:package])
  end
end
