defmodule UpsPackageTrackingClient.Address do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:city, :string)
    field(:country, :string)
    field(:postal_code, :string)
    field(:state_province, :string)
  end

  @type t() :: %UpsPackageTrackingClient.Address{
          city: String.t(),
          country: String.t(),
          postal_code: String.t(),
          state_province: String.t()
        }

  def changeset(address, attrs \\ %{}) do
    attrs =
      attrs
      |> UpsPackageTrackingClient.Map.rename_key("postalCode", "postal_code")
      |> UpsPackageTrackingClient.Map.rename_key("stateProvince", "state_province")

    address
    |> cast(attrs, [:city, :country, :postal_code, :state_province])
    |> validate_length(:city, max: 50)
    |> validate_length(:country, is: 2)
    |> validate_length(:postal_code, max: 15)
  end
end
