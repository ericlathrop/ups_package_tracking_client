defmodule UpsPackageTrackingClient.UpsTime do
  use Ecto.Type
  def type, do: :time

  def cast(
        <<hour_string::binary-size(2), minute_string::binary-size(2),
          second_string::binary-size(2)>>
      ) do
    with {hour, _} <- Integer.parse(hour_string),
         {minute, _} <- Integer.parse(minute_string),
         {second, _} <- Integer.parse(second_string),
         {:ok, time} <- Time.new(hour, minute, second) do
      {:ok, time}
    else
      _ -> :error
    end
  end

  def cast(_), do: :error

  def load(erl_time = {_hour, _minute, _second}), do: {:ok, Time.from_erl(erl_time)}

  def dump(time = %Time{}), do: {:ok, Time.to_erl(time)}
  def dump(_), do: :error
end
