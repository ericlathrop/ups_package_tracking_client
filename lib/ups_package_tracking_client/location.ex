defmodule UpsPackageTrackingClient.Location do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    embeds_one(:address, UpsPackageTrackingClient.Address)
  end

  @type t() :: %UpsPackageTrackingClient.Location{
          address: UpsPackageTrackingClient.Address.t()
        }

  def changeset(location, attrs \\ %{}) do
    location
    |> cast(attrs, [])
    |> cast_embed(:address)
  end
end
