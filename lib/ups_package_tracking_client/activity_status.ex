defmodule UpsPackageTrackingClient.ActivityStatus do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:code, :string)
    field(:description, :string)
    field(:type, :string)
  end

  @type t() :: %UpsPackageTrackingClient.ActivityStatus{
          code: String.t(),
          description: String.t(),
          type: String.t()
        }

  def changeset(activity_status, attrs \\ %{}) do
    activity_status
    |> cast(attrs, [:code, :description, :type])
    # FIXME: docs say code & type are required, but it is missing on some responses
    |> validate_required([:description])
    |> validate_length(:code, max: 10)
  end
end
