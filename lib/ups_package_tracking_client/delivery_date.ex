defmodule UpsPackageTrackingClient.DeliveryDate do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:date, UpsPackageTrackingClient.UpsDate)
    field(:type, :string)
  end

  @type t() :: %UpsPackageTrackingClient.DeliveryDate{
          date: Date.t(),
          type: String.t()
        }

  def changeset(delivery_date, attrs \\ %{}) do
    delivery_date
    |> cast(attrs, [:date, :type])
    |> validate_required([:date, :type])
    |> validate_length(:type, is: 3)
  end
end
