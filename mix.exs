defmodule UpsPackageTrackingClient.MixProject do
  use Mix.Project

  def project do
    [
      app: :ups_package_tracking_client,
      config_path: "config/config.exs",
      deps: deps(),
      description: "Client for the UPS Package Tracking REST/JSON API.",
      docs: docs(),
      elixir: "~> 1.10",
      homepage_url: "https://gitlab.com/ericlathrop/ups_package_tracking_client",
      name: "UpsPackageTrackingClient",
      package: package(),
      source_url: "https://gitlab.com/ericlathrop/ups_package_tracking_client",
      start_permanent: Mix.env() == :prod,
      version: "0.1.2"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ecto, "~> 3.4.6"},
      {:ex_doc, "~> 0.22.2", only: :dev, runtime: false},
      {:httpoison, "~> 1.7.0"},
      {:jason, "~> 1.2.1"},
      {:mix_test_watch, "~> 1.0.2", only: :dev, runtime: false}
    ]
  end

  defp docs do
    [
      main: "readme",
      extras: ["CHANGELOG.md", "README.md"]
    ]
  end

  defp package do
    [
      licenses: ["MIT"],
      links: %{
        "GitLab" => "https://gitlab.com/ericlathrop/ups_package_tracking_client"
      }
    ]
  end
end
