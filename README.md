# UpsPackageTrackingClient

[Hex.pm](https://hex.pm/packages/ups_package_tracking_client) [Documentation](https://hexdocs.pm/ups_package_tracking_client/readme.html)

Client for the UPS Package Tracking REST/JSON API.

## Usage

```elixir
iex(1)> UpsPackageTrackingClient.track("1Z5338FF0107231059", "ACCESS_KEY", "en_US")
{:ok,
 %UpsPackageTrackingClient.TrackingResponse{ ... }
}
```

## Obtaining an API key

Visit the [UPS Developer Kit](https://www.ups.com/upsdeveloperkit?loc=en_US) page, create an account, and request an access key.
